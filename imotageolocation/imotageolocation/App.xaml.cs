﻿using System;
using imotageolocation.Pages;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using imotageolocation.Data;

namespace imotageolocation
{
    public partial class App : Application
    {
        static imotageolocationDatabase database;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Locations());
        }

        protected override async void OnStart()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.LocationWhenInUse>();
            if (status != PermissionStatus.Granted)
            {
                status = await Permissions.RequestAsync<Permissions.LocationWhenInUse>();
            }

        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public static imotageolocationDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new imotageolocationDatabase();
                }
                return database;
            }

        }
    }
}

