﻿using System;
using System.Collections.Generic;
using imotageolocation.ViewModels;
using Shiny;
using Shiny.Notifications;
using Xamarin.Forms;

namespace imotageolocation.Pages
{
    public partial class Locations : ContentPage
    {
        INotificationManager notifications = ShinyHost.Resolve<INotificationManager>();

        public Locations()
        {
            InitializeComponent();
            this.BindingContext = new LocationsViewModel();

            locView.RefreshCommand = new Command(async () => {
                //Do your stuff.
                locView.ItemsSource = await App.Database.GetGeoLogItemsAsync();
                locView.IsRefreshing = false;
            });

            posView.RefreshCommand = new Command(async () => {
                //Do your stuff.
                posView.ItemsSource = await App.Database.GetGeoPosItemsAsync();
                posView.IsRefreshing = false;
            });


        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            locView.ItemsSource = await App.Database.GetGeoLogItemsAsync();
            posView.ItemsSource = await App.Database.GetGeoPosItemsAsync();
        }



    }
}
