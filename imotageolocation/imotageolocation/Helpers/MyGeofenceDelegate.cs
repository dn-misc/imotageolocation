﻿using System;
using System.Threading.Tasks;
using imotageolocation.Models;
using Shiny.Locations;
using Shiny.Notifications;
using Xamarin.Forms;

namespace imotageolocation.Helpers
{
    public class MyGeofenceDelegate : IGeofenceDelegate, IGpsDelegate
    {
        protected Page MainPage => Application.Current.MainPage;
        readonly INotificationManager notifications;
        readonly IGeofenceManager geofences;
        readonly IGpsManager gpsmanager;
        private long _lastUpdate;

        public MyGeofenceDelegate(INotificationManager notifications, IGeofenceManager geofences, IGpsManager gpsmanager)
        {
            this.notifications = notifications;
            this.geofences = geofences;
            this.gpsmanager = gpsmanager;
        }


        public async Task OnStatusChanged(GeofenceState newStatus, GeofenceRegion region)
        {
            if (newStatus == GeofenceState.Entered)
            {
                //await this.notifications.Send(new Notification
                //{
                //    Title = "WELCOME!",
                //    Message = "It is good to have you back " + region.Identifier
                //});

                var state = newStatus.ToString().ToUpper();

                await this.notifications.Send(
                "Geofencing",
                $"You {state} the geofence {region.Identifier}"
                );

                //save to db
                var geoLogItem = new GeoLogItem();
                geoLogItem.Time = DateTime.Now;
                geoLogItem.Location = region.Identifier;
                geoLogItem.Action = "Enter";

                await App.Database.SaveGeoLogItemAsync(geoLogItem);

                Console.WriteLine("GEOREFENECE REGION ENTERED");


            }
            else
            {
                //await this.notifications.Send(new Notification
                //{
                //    Title = "GOODBYE!",
                //    Message = "You will be missed at " + region.Identifier
                //});

                var state = newStatus.ToString().ToUpper();

                await this.notifications.Send(
                "Geofencing",
                $"You {state} the geofence {region.Identifier}"
                );

                //save to db
                var geoLogItem = new GeoLogItem();
                geoLogItem.Time = DateTime.Now;
                geoLogItem.Location = region.Identifier;
                geoLogItem.Action = "Exit";

                await App.Database.SaveGeoLogItemAsync(geoLogItem);

                Console.WriteLine("GEOREFENECE REGION EXITED");

            }
        }

        public async Task OnReading(IGpsReading reading)
        {
            if (DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - _lastUpdate > 10000)
            {
                Console.WriteLine(DateTime.Now + ": " +  reading.Position.Latitude + ", " + reading.Position.Longitude);
                //save position to db
                var geoPosItem = new GeoPosItem();
                geoPosItem.Time = DateTime.Now;
                geoPosItem.Latitude = reading.Position.Latitude;
                geoPosItem.Longitude = reading.Position.Longitude;
                geoPosItem.RaceID = 1;
                geoPosItem.SubRaceID = 2;

                await App.Database.SaveGeoPosItemAsync(geoPosItem);

                _lastUpdate = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;

            }

        }

        protected virtual Task<T> InvokeOnMainThread<T>(Func<Task<T>> func) => Device.InvokeOnMainThreadAsync(func);
        protected virtual Task InvokeOnMainThread(Func<Task> func) => Device.InvokeOnMainThreadAsync(func);
        protected virtual Task InvokeOnMainThread(Action action) => Device.InvokeOnMainThreadAsync(action);

        protected virtual Task Alert(string message, string title = "Info")
           => this.InvokeOnMainThread(() => this.MainPage.DisplayAlert(title, message, "OK"));
    }
}
