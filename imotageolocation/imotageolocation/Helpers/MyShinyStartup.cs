﻿using System;
using imotageolocation.Helpers;
using Microsoft.Extensions.DependencyInjection;
using Shiny;

namespace imotageolocation
{
    public class MyShinyStartup : ShinyStartup
    {
        public override void ConfigureServices(IServiceCollection services, IPlatform platform)
        {
            
            services.UseGeofencing<MyGeofenceDelegate>();

            // if you need realtime geofencing based on full background gps, you can use this.  It will kill your user's battery
            //services.UseGpsDirectGeofencing<GeofenceDelegate>();

            // let's send some notifications from our geofence
            services.UseNotifications();

            // we use this in the example, it isn't needed for geofencing in general
            services.UseGps<MyGeofenceDelegate>();
        }
    }
}

