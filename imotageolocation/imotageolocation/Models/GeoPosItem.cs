﻿using System;
using SQLite;

namespace imotageolocation.Models
{
    public class GeoPosItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DateTime Time { get; set; }
        public Int32 RaceID { get; set; }
        public Int32 SubRaceID { get; set; }
        public Double Latitude { get; set; }
        public Double Longitude { get; set; }
    }
}

