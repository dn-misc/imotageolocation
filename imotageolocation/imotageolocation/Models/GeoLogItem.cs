﻿using System;
using SQLite;

namespace imotageolocation.Models
{
    public class GeoLogItem
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public DateTime Time { get; set; }
        public string Location { get; set; }
        public string Action { get; set; }
    }
}

