﻿using System;
using Shiny;
using Shiny.Locations;
using Shiny.Notifications;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Essentials;
using imotageolocation.Helpers;
using System.Threading.Tasks;

namespace imotageolocation.ViewModels
{
    public class LocationsViewModel : Shiny.NotifyPropertyChanged
    {
        //IDisposable gpsListener;
        protected Page MainPage => Application.Current.MainPage;

        // shiny doesn't usually manage your viewmodels, so we'll do this for now
        IGeofenceManager geofences = ShinyHost.Resolve<IGeofenceManager>();
        readonly IGpsManager gpsmanager = ShinyHost.Resolve<IGpsManager>();
        INotificationManager notifications = ShinyHost.Resolve<INotificationManager>();
        string _rangingStatus;
        public string RangingStatus
        {
            get => this._rangingStatus;
            set => this.Set(ref this._rangingStatus, value);
        }


        public LocationsViewModel()
        {
            RangingStatus = GlobalVarsBind.Current.GeoFenceStatus;

            //if (gpsmanager.IsListening())
            //    RangingStatus = "Listening";
            //else
            //    RangingStatus = "Inactive";

            LocStart = new Command(async () =>
            {
                this.RangingStatus = "Ranging ...";
                GlobalVarsBind.Current.GeoFenceStatus = "Ranging ...";

                //if (this.gpsmanager.CurrentListener != null)
                //{
                //    await this.gpsmanager.StopListener();
                //}
                //else
                //{
                //    // THIS IS GPS Initialization

                //    var result = await this.gpsmanager.RequestAccess(new GpsRequest { BackgroundMode = GpsBackgroundMode.Realtime });

                //    if (result != AccessState.Available)
                //    {
                //        await this.Alert("Insufifficient permissions!");
                //        return;
                //    }

                //    var request = new GpsRequest
                //    {
                //        Accuracy = GpsAccuracy.Normal,
                //        BackgroundMode = GpsBackgroundMode.Realtime
                //    };

                //    try
                //    {
                //        await this.gpsmanager.StartListener(request);
                //    }
                //    catch (Exception ex)
                //    {
                //        await this.Alert(ex.ToString());
                //    }
                //}



                // this is really only required on iOS, but do it to be safe
                var access = await geofences.RequestAccess();

                if (access != AccessState.Available)
                {
                    Console.WriteLine("Insufficient permissions");
                }
                else
                {
                    {
                        RangingStatus = "Ranging ...";

                        Console.WriteLine("geofence added region");
                        await this.geofences.StartMonitoring(new GeofenceRegion(
                            "Jarun - Rollers",
                            new Position(45.787729, 15.912042),
                            Distance.FromMeters(100)
                        )
                        {
                            NotifyOnEntry = true,
                            NotifyOnExit = true,
                            SingleUse = false
                        });
                        await this.geofences.StartMonitoring(new GeofenceRegion(
                            "Jarun - Island",
                            new Position(45.785180, 15.912293),
                            Distance.FromMeters(100)
                        )
                        {
                            NotifyOnEntry = true,
                            NotifyOnExit = true,
                            SingleUse = false
                        });
                        await this.geofences.StartMonitoring(new GeofenceRegion(
                            "Jarun - Multisensory park",
                            new Position(45.777719, 15.916001),
                            Distance.FromMeters(100)
                        )
                        {
                            NotifyOnEntry = true,
                            NotifyOnExit = true,
                            SingleUse = false
                        });
                        await this.geofences.StartMonitoring(new GeofenceRegion(
                            "Jarun - Boccia",
                            new Position(45.777036, 15.937844),
                            Distance.FromMeters(100)
                        )
                        {
                            NotifyOnEntry = true,
                            NotifyOnExit = true,
                            SingleUse = false
                        });
                        await this.geofences.StartMonitoring(new GeofenceRegion(
                            "Jarun - Restaurant",
                            new Position(45.781944, 15.925255),
                            Distance.FromMeters(100)
                        )
                        {
                            NotifyOnEntry = true,
                            NotifyOnExit = true,
                            SingleUse = false
                        });

                    }
                }

                //gpsmanager.Title = "ImotaGeolocation";
                //gpsmanager.Message = "Loooking for geo points in background";

            });

            LocStop = new Command(async () =>
            {
                await gpsmanager.StopListener();
                await geofences.StopAllMonitoring();
                this.RangingStatus = "Stopped...";
                GlobalVarsBind.Current.GeoFenceStatus = "Stopped...";
            });

            DBCleanup = new Command(async () =>
            {
                await App.Database.DeleteGeoLogRecords();
                await App.Database.DeleteGeoPosRecords();
            });
        }

        public ICommand LocStart { get; }
        public ICommand LocStop { get; }
        public ICommand DBCleanup { get; }

        protected virtual Task<T> InvokeOnMainThread<T>(Func<Task<T>> func) => Device.InvokeOnMainThreadAsync(func);
        protected virtual Task InvokeOnMainThread(Func<Task> func) => Device.InvokeOnMainThreadAsync(func);
        protected virtual Task InvokeOnMainThread(Action action) => Device.InvokeOnMainThreadAsync(action);

        protected virtual Task Alert(string message, string title = "Info")
           => this.InvokeOnMainThread(() => this.MainPage.DisplayAlert(title, message, "OK"));
    }
}


