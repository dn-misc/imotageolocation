﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using imotageolocation.Helpers;
using imotageolocation.Models;
using SQLite;
using System.Linq;

namespace imotageolocation.Data
{
    public class imotageolocationDatabase
    {
        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public imotageolocationDatabase()
        {
            InitializeAsync(); //.SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(GeoLogItem).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(GeoLogItem)).ConfigureAwait(false);
                    initialized = true;
                }
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(GeoPosItem).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(GeoPosItem)).ConfigureAwait(false);
                    initialized = true;
                }
            }
        }

        public Task<List<GeoLogItem>> GetGeoLogItemsAsync()
        {
            return Database.Table<GeoLogItem>().ToListAsync();
        }

        public Task<List<GeoPosItem>> GetGeoPosItemsAsync()
        {
            return Database.Table<GeoPosItem>().ToListAsync();
        }

        public Task<GeoLogItem> GetItemAsync(int id)
        {
            return Database.Table<GeoLogItem>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveGeoLogItemAsync(GeoLogItem item)
        {
            if (item.ID != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> SaveGeoPosItemAsync(GeoPosItem item)
        {
            if (item.ID != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public async Task DeleteGeoLogRecords()
        {
            await Database.Table<GeoLogItem>().DeleteAsync(x => x.Time < DateTime.Now);
        }

        public async Task DeleteGeoPosRecords()
        {
            await Database.Table<GeoPosItem>().DeleteAsync(x => x.Time < DateTime.Now);
        }
    }
}
